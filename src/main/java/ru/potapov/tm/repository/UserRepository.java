package ru.potapov.tm.repository;

import ru.potapov.tm.entity.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserRepository extends AbstractRepository<User> {
    private Map<String, User> mapUser = new HashMap<>();

    @Override
    public Collection<User> findAll() {
        return mapUser.values();
    }

    @Override
    public User findOne(String name) {
        for (User user : mapUser.values()) {
            if (user.getLogin().equals(name))
                return user;
        }
        return null;
    }

    public User findOneById(String id) {
        for (User user : mapUser.values()) {
            if (user.getId().equals(id))
                return user;
        }
        return null;
    }

    @Override
    public void persist(User user) {
        mapUser.putIfAbsent(user.getId(), user);
    }

    @Override
    public User merge(User userNew) {
        mapUser.put(userNew.getId(), userNew);
        return userNew;
    }

    @Override
    public void remove(User user) {
        mapUser.remove(user.getId());
    }

    @Override
    public void removeAll() {
        mapUser.clear();
    }

    @Override
    public void removeAll(Collection<User> list) {
        for (User user : list) {
            mapUser.remove(user.getId());
        }
    }

    public Collection<User> getCollectionUser(){
        return mapUser.values();
    }
}
