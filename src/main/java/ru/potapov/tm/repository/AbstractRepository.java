package ru.potapov.tm.repository;

import java.util.Collection;

public abstract class AbstractRepository<T>  {
    public abstract Collection<T> findAll();
    public abstract T findOne(String name);
    public abstract void persist(T t);
    public abstract T merge(T tNew);
    public abstract void remove(T t);
    public abstract void removeAll();
    public abstract void removeAll(Collection<T> list);
}
