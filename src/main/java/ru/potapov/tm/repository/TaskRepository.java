package ru.potapov.tm.repository;

import ru.potapov.tm.entity.Task;

import java.util.*;

public class TaskRepository extends AbstractRepository<Task>{
    private Map<String, Task> mapTask = new HashMap<>();;

    public TaskRepository() {}

    @Override
    public Collection findAll() {
        return mapTask.values();
    }

    @Override
    public Task findOne(String name) {
        for (Task task : mapTask.values()) {
            if (task.getName().equals(name)){
                return task;
            }
        }
        return null;
    }

    @Override
    public void persist(Task task) {
        mapTask.putIfAbsent(task.getId(), task);
    }

    @Override
    public Task merge(Task taskNew) {
        mapTask.put(taskNew.getId(), taskNew);
        return taskNew;
    }

    @Override
    public void remove(Task task) {
        mapTask.remove(task.getId());
    }

    @Override
    public void removeAll() {
        mapTask.clear();
    }

    @Override
    public void removeAll(Collection<Task> list) {
        for (Task task : list) {
            mapTask.remove(task.getId());
        }
    }

    public Collection<Task> getCollectionTasks(){
        return mapTask.values();
    }
}
