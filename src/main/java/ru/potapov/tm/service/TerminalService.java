package ru.potapov.tm.service;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.command.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TerminalService {
    private Map<String, AbstractCommand> mapCommand = new LinkedHashMap<>();
    private Bootstrap bootstrap;

    private DateTimeFormatter   ft                  = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    private Scanner             in                  = new Scanner(System.in);

    //Constants
    private final String YY                         = "Y";
    private final String Yy                         = "y";

    public TerminalService(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        regestry(new DelimiterCommand(null));
        regestry(new ExitCommand(bootstrap));
        regestry(new HelpCommand(bootstrap));
        regestry(new DelimiterCommand(null));
        regestry(new ProjectCreateCommand(bootstrap));
        regestry(new ProjectReadCommand(bootstrap));
        regestry(new ProjectUpdateCommand(bootstrap));
        regestry(new ProjectDeleteCommand(bootstrap));
        regestry(new ProjectDeleteAllCommand(bootstrap));
        regestry(new DelimiterCommand(null));
        regestry(new TaskCreateCommand(bootstrap));
        regestry(new TaskReadAllCommand(bootstrap));
        regestry(new TaskReadCommand(bootstrap));
        regestry(new TaskUpdateCommand(bootstrap));
        regestry(new TaskDeleteCommand(bootstrap));
        regestry(new TaskDeleteForProjectCommand(bootstrap));
        regestry(new TaskDeleteAllCommand(bootstrap));
        regestry(new DelimiterCommand(null));

        regestry(new UserAuthorizeCommand(bootstrap));
        regestry(new UserExitCommand(bootstrap));

        regestry(new UserCreateCommand(bootstrap));
        regestry(new UserReadAllCommand(bootstrap));
        regestry(new UserReadCommand(bootstrap));
        regestry(new UserUpdateCommand(bootstrap));
        regestry(new UserDeleteCommand(bootstrap));
    }

    private void regestry(AbstractCommand command){
        if ( Objects.isNull(command))
            return;

        final String commandName        = command.getName();
        final String commandDescription = command.getDescription();

        if (Objects.isNull(commandName) || Objects.isNull(commandDescription)
                || commandName.isEmpty() || commandDescription.isEmpty())
            return;

        command.setServiceLocator(bootstrap);
        mapCommand.put(commandName, command);
    }

    public Collection<AbstractCommand> getListCommands(){
        return mapCommand.values();
    }

    public Map<String, AbstractCommand> getMapCommands(){
        return mapCommand;
    }

    public LocalDateTime inputDate(String massage){
        printlnArbitraryMassage(massage);
        String strDate = in.nextLine();
        LocalDateTime date;
        try {
            int year = Integer.parseInt(strDate.substring(6)),
                    month= Integer.parseInt(strDate.substring(3,5)),
                    day  = Integer.parseInt(strDate.substring(0,2));
            date = LocalDateTime.of(year,month, day,0,0);
        }catch (Exception e){
            printlnArbitraryMassage("Error formate date! Date set to the end of year.");
            date = LocalDateTime.of(LocalDateTime.now().getYear(),12,31,23,59);
        }

        return date;
    }

    public Scanner getIn() {
        return in;
    }

    public DateTimeFormatter getFt() {
        return ft;
    }

    public String readLine(String msg){
        System.out.println(msg);
        return in.nextLine();
    }

    public void printMassageNotAuthorized(){
        printlnArbitraryMassage("You are not authorized, plz login (type command <user-login>)");
    }

    public void printMassageCompleted(){
        printlnArbitraryMassage("Completed");
    }

    public void printMassageOk(){
        printlnArbitraryMassage("Ok");
    }

    public void printlnArbitraryMassage(String msg){
        printArbitraryMassage(msg + "\n");
    }

    public void printArbitraryMassage(String msg){
        System.out.print(msg);
    }

//    public void setBootstrap(bootstrap bootstrap) {
//        this.bootstrap = bootstrap;
//    }
}
