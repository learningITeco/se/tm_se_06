package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.ProjectRepository;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public int checkSize(){
        return projectRepository.getCollectionProject().size();
    }

    public Project findProjectByName(String name){
        Project result = null;

        for (Project project : projectRepository.getCollectionProject()) {
            if (project.getName().equals(name)) {
                result = project;
                break;
            }
        }
        return result;
    }

    public Collection<Project> getCollectionProjects(User user){
        Collection<Project> projects = new ArrayList<>();
        for (Project project : projectRepository.getCollectionProject()) {
            if (project.getUserId().equals(user.getId()))
                projects.add(project);
        }
        return projects;
    }

    public Project renameProject(Project project, String name) throws CloneNotSupportedException{
        if (Objects.nonNull(name)){
            Project newProject = (Project) project.clone();
            newProject.setName(name);
            return projectRepository.merge(newProject);
        }
        return project;
    }
    public void removeAll(User user){
        Collection<Project> projectsForRemoving = new ArrayList<>();
        for (Project project : projectRepository.getCollectionProject()) {
            if (project.getUserId().equals(user.getId()))
                projectsForRemoving.add(project);
        }
        removeAll(projectsForRemoving);
    }

    public void removeAll(Collection<Project> listProjects){
        projectRepository.removeAll(listProjects);
    }

    public void remove(Project project){
        projectRepository.remove(project);
    }

    public void put(Project project){
        projectRepository.merge(project);
    }

    public String collectProjectInfo(Project project, String owener, DateTimeFormatter ft){
        String res = "";

        res += "\n";
        res += "Project [" + project.getName() + "]" +  "\n";
        res += "Owner: " + owener + "\n";
        res += "Description: " + project.getDescription() + "\n";
        res += "ID: " + project.getId() + "\n";
        res += "Date start: " + project.getDateStart().format(ft) + "\n";
        res += "Date finish: " + project.getDateFinish().format(ft) + "\n";

        return res;
    }
}
