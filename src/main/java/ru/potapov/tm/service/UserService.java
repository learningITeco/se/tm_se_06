package ru.potapov.tm.service;

import ru.potapov.tm.entity.RoleType;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.UserRepository;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

public class UserService {
    private UserRepository userRepository;
    private MessageDigest md                = null;

    private boolean isAuthorized            = false;
    private User authorizedUser             = null;

    public UserService() {
        //digest (MD5):
        try {
            md = MessageDigest.getInstance("MD5");
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
    }

    public UserService(UserRepository userRepository) {
        this();
        this.userRepository = userRepository;
        createPredefinedUsers();
    }

    public User create(String name, String hashPass, RoleType role){
        User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        userRepository.persist(user);

        return user;
    }

    public User getUserByName(String name){
        return userRepository.findOne(name);
    }

    public User getUserById(String id){
        return userRepository.findOneById(id);
    }

    public boolean isUserPassCorrect(User user, String hashPass){
        boolean res = false;

        //if ( Arrays.equals(user.getHashPass(), hashPass) )
        if (user.getHashPass().equals(hashPass))
            res = true;

        return res;
    }

    public Collection<User> getCollectionUser(){
        return userRepository.getCollectionUser();
    }

    public User changePass(User user, String newHashPass) throws CloneNotSupportedException{
        User newUser = (User) user.clone();
        newUser.setHashPass(newHashPass);
        return userRepository.merge(newUser);
    }

    public void put(User user){
        userRepository.persist(user);
    }

    public String collectUserInfo(User user){
        String res = "";

        res += "\n";
        res += "    User [" + user.getLogin() + "]" +  "\n";
        res += "    ID: " + user.getId() + "\n";
        res += "    Role: " + user.getRoleType() + "\n";

        return res;
    }

    private void createPredefinedUsers() {
        String hashPass;
        byte [] hashPassByte;

        hashPassByte = md.digest("1".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("user",  hashPass, RoleType.User);

        hashPassByte = md.digest("2".getBytes());
        hashPass = DatatypeConverter.printHexBinary(hashPassByte);
        create("admin", hashPass, RoleType.Administrator);
    }

    public MessageDigest getMd() {
        return md;
    }

    public boolean isAuthorized() {
        return isAuthorized;
    }

    public void setAuthorized(boolean authorized) {
        isAuthorized = authorized;
    }

    public User getAuthorizedUser() {
        return authorizedUser;
    }

    public void setAuthorizedUser(User authorizedUser) {
        this.authorizedUser = authorizedUser;
    }
}
