package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.repository.ProjectRepository;
import ru.potapov.tm.repository.TaskRepository;

import java.time.format.DateTimeFormatter;
import java.util.*;

public class TaskService {
    private TaskRepository      taskRepository;
    private ProjectRepository   projectRepository;

    public TaskService() {}

    public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public int checkSize(){
         return taskRepository.getCollectionTasks().size();
    }

    public Task findTaskByName(String name){
        Task result = null;

        for (Task task : taskRepository.getCollectionTasks()) {
            if (task.getName().equals(name)) {
                result = task;
                break;
            }
        }
        return result;
    }

    public void remove(Task task){
        taskRepository.remove(task);
    }

    public void removeAll(User user){
        Collection<Task> tasksForRemoving = new ArrayList<>();
        for (Task task : taskRepository.getCollectionTasks()) {
            if (task.getUserId().equals(user.getId()))
                tasksForRemoving.add(task);
        }
        taskRepository.removeAll(tasksForRemoving);
    }

    public void removeAll(Collection<Task> listTasks){
        taskRepository.removeAll(listTasks);
    }

    public Task renameTask(Task task, String name) throws CloneNotSupportedException{

        if (Objects.nonNull(name)){
            Task newTask = (Task) task.clone();
            newTask.setName(name);
            return taskRepository.merge(newTask);
        }
        return task;
    }

    public void changeProject(Task task, Project project) throws CloneNotSupportedException{
        if (Objects.nonNull(project)){
            Task newTask = (Task) task.clone();
            newTask.setProjectId(project.getId());
            taskRepository.merge(newTask);
        }
    }

    public Collection<Task> findAll(String idProject){
        Collection<Task> listTask = new ArrayList<>();
        for (Task task : taskRepository.getCollectionTasks()) {
            if (task.getProjectId().equals(idProject)){
                listTask.add(task);
            }
        }
        return listTask;
    }

    public void put(Task task){
        taskRepository.merge(task);
    }

    public String collectTaskInfo(Project project, Task task, DateTimeFormatter ft){
        String res = "";

        res += "\n";
        res += "    Task [" + task.getName() + "]" +  "\n";
        res += "    Description: " + task.getDescription() + "\n";
        res += "    ID: " + task.getId() + "\n";
        res += "    Date start: " + task.getDateStart().format(ft) + "\n";
        res += "    Date finish: " + task.getDateFinish().format(ft) + "\n";

        return res;
    }
}
