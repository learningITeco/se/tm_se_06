package ru.potapov.tm.entity;

public class User implements Cloneable{
    private String      id;
    private String      login;
    private String      hashPass;
    private RoleType    roleType = RoleType.User;

    public User() {
    }

    public User(String login) {
        this();
        this.login = login;
    }

    public String displayName(){
        return "" + roleType + ": " + login;
    }

    public String getLogin() {
        return login;
    }

    public String getHashPass() {
        return hashPass;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setHashPass (String hashPass) {
        this.hashPass = hashPass;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
