package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;

import java.util.Objects;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {
    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Creates a new project";
    }

    @Override
    public void execute() {
        super.execute();
        Project findProject = null;
        boolean circleForName = true;
        while (circleForName){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a name for a new project:");
            String name = getServiceLocator().getTerminalService().getIn().nextLine();

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.nonNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with the name [" + name + "] already exist");
                continue;
            }

            circleForName = false;
            getServiceLocator().getTerminalService().printMassageOk();
            findProject = new Project(name);
        }

        getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a description for this project:");
        findProject.setDescription(getServiceLocator().getTerminalService().getIn().nextLine());

        //Date:
        findProject.setDateStart(getServiceLocator().getTerminalService().inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        findProject.setDateFinish(getServiceLocator().getTerminalService().inputDate("Input a finish date for new project:"));

        findProject.setId(UUID.randomUUID().toString());
        findProject.setUserId(getServiceLocator().getUserService().getAuthorizedUser().getId());

        getServiceLocator().getTerminalService().printMassageCompleted();
        getServiceLocator().getProjectService().put(findProject);

        User owner = getServiceLocator().getUserService().getUserById(findProject.getUserId());
        String projectInfo = getServiceLocator().getProjectService().collectProjectInfo(findProject, owner.getLogin(), getServiceLocator().getTerminalService().getFt());
        getServiceLocator().getTerminalService().printlnArbitraryMassage(projectInfo);
    }
}
