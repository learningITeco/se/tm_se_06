package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;

import java.util.Objects;

public class TaskReadCommand extends TaskReadCommandAbstract {
    public TaskReadCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-read";
    }

    @Override
    public String getDescription() {
        return "Reads tasks for a current project";
    }

    @Override
    public void execute() {
        super.execute();
        if (getBootstrap().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We have not any project");
            return;
        }

        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a project name for reading its tasks:");
            String name = getServiceLocator().getTerminalService().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getBootstrap().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }
        printTasksOfProject(findProject, getBootstrap().getTaskService().findAll(findProject.getId()));
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
