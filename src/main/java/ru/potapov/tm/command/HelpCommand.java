package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class HelpCommand extends AbstractCommand {
    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Lists all command";
    }

    @Override
    public void execute() {
        super.execute();
        for (final AbstractCommand command : getServiceLocator().getTerminalService().getListCommands()) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage(command.getName() + ": " + command.getDescription());
        }
    }
}
