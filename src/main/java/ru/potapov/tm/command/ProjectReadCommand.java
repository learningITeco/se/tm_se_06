package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.User;

public class ProjectReadCommand extends AbstractCommand {
    public ProjectReadCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-read";
    }

    @Override
    public String getDescription() {
        return "Reads all projects";
    }

    @Override
    public void execute() {
        super.execute();
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All projects: \n");
        if (getServiceLocator().getProjectService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We have not any project");
            return;
        }

        User owner = null;
        for (Project project : getServiceLocator().getProjectService().getCollectionProjects( getServiceLocator().getUserService().getAuthorizedUser() )) {
            owner = getServiceLocator().getUserService().getUserById(project.getUserId());

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(project.getUserId())){
                continue;
            }

            String projectInfo = getServiceLocator().getProjectService().collectProjectInfo(project, owner.getLogin(), getServiceLocator().getTerminalService().getFt());
            getServiceLocator().getTerminalService().printlnArbitraryMassage(projectInfo);
        }
    }
}
