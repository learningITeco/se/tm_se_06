package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

import javax.xml.bind.DatatypeConverter;
import java.util.Objects;

public class UserUpdateCommand extends AbstractCommand {
    public UserUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Updates a user password";
    }

    @Override
    public void execute() {
        super.execute();
        User findUser = null;

        boolean circleForName = true;
        while (circleForName){
            String name = getServiceLocator().getTerminalService().readLine("Input a user name for update:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.isNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }


        String pass = getServiceLocator().getTerminalService().readLine("Input a new password for user ["+ findUser.getLogin() + "]:");
        try {
            getServiceLocator().getUserService().changePass(findUser, DatatypeConverter.printHexBinary(getServiceLocator().getUserService().getMd().digest(pass.getBytes())) );
        }catch (Exception e) {e.printStackTrace();}
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
