package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.service.UserService;

import javax.xml.bind.DatatypeConverter;
import java.util.Objects;

public class UserAuthorizeCommand extends AbstractCommand {
    private UserService userService = null;

    public UserAuthorizeCommand(Bootstrap bootstrap) {
        super(bootstrap);
        userService = bootstrap.getUserService();
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "Logins user in Task-manager";
    }

    @Override
    public void execute() {
        super.execute();
        String userInput = "";
        while ( true ){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Type your login name or the command <exit>");
            if ("exit".equals(userInput)){
                return;
            }

            //User name
            userInput = getServiceLocator().getTerminalService().readLine("login: ");

            User user = userService.getUserByName(userInput);
            if ( Objects.isNull(user) ){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed user name!");
                continue;
            }

            //User pass
            String hashPass = "";
            userInput = getServiceLocator().getTerminalService().readLine("pass: ");


//            Console console = System.console();
//            char[] chUserInput = console.readPassword("pass:");//getServiceLocator().getIn().;
//            System.out.print("pass: ");
//            System.out.println(chUserInput.toString());
//            userInput = chUserInput.toString();
            hashPass = DatatypeConverter.printHexBinary(getServiceLocator().getUserService().getMd().digest(userInput.getBytes()));

            if ( !userService.isUserPassCorrect(user, hashPass) ){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Failed the user password!");
                continue;
            }

            getServiceLocator().getUserService().setAuthorized(true);
            getServiceLocator().getUserService().setAuthorizedUser(user);
            getServiceLocator().getTerminalService().printMassageCompleted();
            return;
        }
    }
}
