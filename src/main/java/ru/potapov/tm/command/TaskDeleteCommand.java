package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Task;

import java.util.Objects;

public class TaskDeleteCommand extends AbstractCommand {
    public TaskDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-delete";
    }

    @Override
    public String getDescription() {
        return "Deletes a task of a project";
    }

    @Override
    public void execute() {
        super.execute();
        if (!getServiceLocator().getUserService().isAuthorized()){
            getServiceLocator().getTerminalService().printMassageNotAuthorized();
            return;
        }

        if (getServiceLocator().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            String name = getServiceLocator().getTerminalService().readLine("Input a task name for remove:");

            if ("exit".equals(name)){
                return;
            }

            findTask = getServiceLocator().getTaskService().findTaskByName(name);

            if (Objects.isNull(findTask)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findTask.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Tsk with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        getServiceLocator().getTaskService().remove(findTask);
        getServiceLocator().getTerminalService().printlnArbitraryMassage("The task has deleted");
    }
}
