package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;

public class TaskReadAllCommand extends TaskReadCommandAbstract {
    public TaskReadAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-read-all";
    }

    @Override
    public String getDescription() {
        return "Reads all tasks";
    }

    @Override
    public void execute() {
        super.execute();
        if (getServiceLocator().getTaskService().checkSize() == 0) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any tasks");
            return;
        }

        for (Project project : getServiceLocator().getProjectService().getCollectionProjects( getServiceLocator().getUserService().getAuthorizedUser() )) {
            printTasksOfProject(project, getServiceLocator().getTaskService().findAll(project.getId()));
        }
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
