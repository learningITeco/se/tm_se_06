package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class TaskDeleteAllCommand extends AbstractCommand {
    public TaskDeleteAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-delete-all";
    }

    @Override
    public String getDescription() {
        return "Deletes all tasks of all projects";
    }

    @Override
    public void execute() {
        super.execute();
        getServiceLocator().getTaskService().removeAll(getServiceLocator().getUserService().getAuthorizedUser());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All tasks of all projects have deleted");
    }
}
