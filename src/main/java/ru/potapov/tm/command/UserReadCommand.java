package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

import java.util.Objects;

public class UserReadCommand extends AbstractCommand {
    public UserReadCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-read";
    }

    @Override
    public String getDescription() {
        return "Reads a user in detail";
    }

    @Override
    public void execute() {
        super.execute();
        User findUser = null;
        boolean circleForProject = true;
        while (circleForProject) {
            String name = getServiceLocator().getTerminalService().readLine("Input a user name:");

            if ("exit".equals(name)){
                return;
            }

            findUser = getServiceLocator().getUserService().getUserByName(name);

            if (Objects.isNull(findUser)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("User with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }
        String userInfo = getServiceLocator().getUserService().collectUserInfo(findUser);
        getServiceLocator().getTerminalService().printlnArbitraryMassage(userInfo);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
