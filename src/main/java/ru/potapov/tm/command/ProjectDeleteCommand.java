package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Objects;

public class ProjectDeleteCommand extends AbstractCommand {
    public ProjectDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-delete";
    }

    @Override
    public String getDescription() {
        return "Deletes the project";
    }

    @Override
    public void execute() {
        super.execute();
        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage("Input a project name for removing:");
            String name = getServiceLocator().getTerminalService().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = getServiceLocator().getTaskService().findAll(findProject.getId());
        getServiceLocator().getTaskService().removeAll(listTaskRemoving);
        getServiceLocator().getProjectService().remove(findProject);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
