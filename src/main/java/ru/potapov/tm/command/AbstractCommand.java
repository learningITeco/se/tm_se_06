package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.bootstrap.ServiceLocator;

public abstract class AbstractCommand {
    private boolean needAuthorize           = false;
    private ServiceLocator serviceLocator;
    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();
    public abstract String getDescription();
    public void execute(){
        if ( needAuthorize && !getServiceLocator().getUserService().isAuthorized() ){
            getServiceLocator().getTerminalService().printMassageNotAuthorized();
            return;
        }
    }

    public void setServiceLocator(ServiceLocator serviceLocator){ this.serviceLocator = serviceLocator; }
    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setBootstrap(Bootstrap bootstrap) { this.bootstrap = bootstrap; }
    public Bootstrap getBootstrap() { return bootstrap; }

    public void setNeedAuthorize(boolean needAuthorize) { this.needAuthorize = needAuthorize; }
    public boolean isNeedAuthorize() { return needAuthorize; }
}
