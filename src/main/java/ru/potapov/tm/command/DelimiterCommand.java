package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class DelimiterCommand extends AbstractCommand {
    static int i        = 1;
    private int counter = 0;

    public DelimiterCommand(Bootstrap bootstrap) {
        super(bootstrap);
        counter = i++;
    }

    @Override
    public String getName() {
        return "--------group " + counter;
    }

    @Override
    public String getDescription() {
        return "-------------";
    }

    @Override
    public void execute() {

    }
}
