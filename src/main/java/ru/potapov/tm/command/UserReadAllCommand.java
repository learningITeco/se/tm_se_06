package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.User;

public class UserReadAllCommand extends AbstractCommand {
    public UserReadAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-read-all";
    }

    @Override
    public String getDescription() {
        return "Lists all users";
    }

    @Override
    public void execute() {
        super.execute();
        getServiceLocator().getTerminalService().printlnArbitraryMassage("All users:");
        for (User user : getServiceLocator().getUserService().getCollectionUser()) {
            getServiceLocator().getTerminalService().printlnArbitraryMassage(user.getLogin() + " [" + user.getRoleType() + "]");
        }
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
