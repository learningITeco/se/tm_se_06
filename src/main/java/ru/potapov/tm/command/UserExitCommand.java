package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class UserExitCommand extends AbstractCommand {
    public UserExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-exit";
    }

    @Override
    public String getDescription() {
        return "Ends user sassion";
    }

    @Override
    public void execute() {
        super.execute();
        getServiceLocator().getUserService().setAuthorized(false);
        getServiceLocator().getUserService().setAuthorizedUser(null);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
