package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Objects;

public class TaskUpdateCommand extends AbstractCommand {
    private final String YY = "Y";
    private final String Yy = "y";

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "Update name or id-projecr of a task";
    }

    @Override
    public void execute() {
        super.execute();
        if (getServiceLocator().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            String name = getServiceLocator().getTerminalService().readLine("Input a task name for update:");

            if ("exit".equals(name)){
                return;
            }

            findTask = getServiceLocator().getTaskService().findTaskByName(name);

            if (Objects.isNull(findTask)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findTask.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Tsk with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }
        }

        String answer = getServiceLocator().getTerminalService().readLine("Do you want to update the name? <y/n>");
        switch (answer){
            case YY:
            case Yy:
                String name = getServiceLocator().getTerminalService().readLine("Input a new name for this task:");
                try {
                    findTask = getServiceLocator().getTaskService().renameTask(findTask, name);
                    getServiceLocator().getTerminalService().printMassageCompleted();
                }catch (Exception e){ e.printStackTrace(); }
        }

        answer = getServiceLocator().getTerminalService().readLine("Do you want to update the project for this task? <y/n>");
        switch (answer){
            case YY:
            case Yy:
                Project findProject = null;
                String name         = getServiceLocator().getTerminalService().readLine("Input the name of project for this task:");
                try {
                    findProject = getServiceLocator().getProjectService().findProjectByName(name);
                }catch (Exception e){ e.printStackTrace();}

                if (Objects.isNull(findProject)){
                    getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] is not exist, project for this task does not changed");
                }else {
                    try {
                        getServiceLocator().getTaskService().changeProject(findTask, findProject);
                    }catch (Exception e){ e.printStackTrace(); }
                    getServiceLocator().getTerminalService().printMassageCompleted();
                }
        }
    }
}
