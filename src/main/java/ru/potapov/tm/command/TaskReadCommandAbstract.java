package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;

public abstract class TaskReadCommandAbstract extends AbstractCommand {
    public TaskReadCommandAbstract(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public abstract String getName();

    @Override
    public abstract String getDescription();

    @Override
    public void execute(){
        super.execute();
    };

    public void printTasksOfProject(Project project, Collection<Task> listTasks){
        getServiceLocator().getTerminalService().printlnArbitraryMassage("");
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (task.getProjectId().equals(project.getId())){
                String taskInfo =  getServiceLocator().getTaskService().collectTaskInfo(project, task, getServiceLocator().getTerminalService().getFt());
                getServiceLocator().getTerminalService().printlnArbitraryMassage(taskInfo);
            }
        }
    }
}
