package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Objects;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand {
    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Creates a new task";
    }

    @Override
    public void execute() {
        super.execute();
        Project findProject = null;
        Task newTask = new Task();


        boolean circleForProject = true;
        while (circleForProject) {
            String name = getServiceLocator().getTerminalService().readLine("Input the name of the project for this task:");

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with the name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
            newTask.setProjectId(findProject.getId());
            getServiceLocator().getTerminalService().printlnArbitraryMassage("OK");
        }


        boolean circleForName = true;
        while (circleForName){
            String name = getServiceLocator().getTerminalService().readLine("Input a name for a new task:");

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;

            Task findTask = getServiceLocator().getTaskService().findTaskByName(name);


            if (Objects.nonNull(findTask)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Task with the name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            getServiceLocator().getTerminalService().printlnArbitraryMassage("OK");
            newTask.setName(name);
        }

        String description = getServiceLocator().getTerminalService().readLine("Input a description for this task:");
        newTask.setDescription(description);

        //Date:
        newTask.setDateStart(getServiceLocator().getTerminalService().inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        newTask.setDateFinish(getServiceLocator().getTerminalService().inputDate("Input a finish date for new project:"));

        newTask.setId(UUID.randomUUID().toString());
        newTask.setUserId(getServiceLocator().getUserService().getAuthorizedUser().getId());

        getServiceLocator().getTaskService().put(newTask);
        getServiceLocator().getTerminalService().printlnArbitraryMassage("");
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Task [" + newTask.getName() + "] has created");
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Project: " + findProject.getName());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Description: " + newTask.getDescription());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("ID: " + newTask.getId());
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Date start: " + newTask.getDateStart().format(getServiceLocator().getTerminalService().getFt()));
        getServiceLocator().getTerminalService().printlnArbitraryMassage("Date finish: " + newTask.getDateFinish().format(getServiceLocator().getTerminalService().getFt()));
    }
}
