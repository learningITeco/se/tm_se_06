package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Objects;

public class TaskDeleteForProjectCommand extends AbstractCommand {
    public TaskDeleteForProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-delete-p";
    }

    @Override
    public String getDescription() {
        return "Deletes all tasks of a project";
    }

    @Override
    public void execute() {
        super.execute();
        if (getServiceLocator().getTaskService().checkSize() == 0){
            getServiceLocator().getTerminalService().printlnArbitraryMassage("We do not have any task.");
            return;
        }

        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            String name = getServiceLocator().getTerminalService().readLine("Input a project name for removing its tasks:");

            if ("exit".equals(name)){
                return;
            }

            findProject = getServiceLocator().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            if (!getServiceLocator().getUserService().getAuthorizedUser().getId().equals(findProject.getUserId())){
                getServiceLocator().getTerminalService().printlnArbitraryMassage("Project with name [" + name + "] does not belong to you!, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = getServiceLocator().getTaskService().findAll(findProject.getId());

        getServiceLocator().getTaskService().removeAll(listTaskRemoving);
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
