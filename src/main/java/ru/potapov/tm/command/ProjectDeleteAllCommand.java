package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class ProjectDeleteAllCommand extends AbstractCommand {
    public ProjectDeleteAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "projects-delete-all";
    }

    @Override
    public String getDescription() {
        return "Deletes all project";
    }

    @Override
    public void execute() {
        super.execute();
        getServiceLocator().getTaskService().removeAll( getServiceLocator().getUserService().getAuthorizedUser() );
        getServiceLocator().getProjectService().removeAll( getServiceLocator().getUserService().getAuthorizedUser() );
        getServiceLocator().getTerminalService().printMassageCompleted();
    }
}
