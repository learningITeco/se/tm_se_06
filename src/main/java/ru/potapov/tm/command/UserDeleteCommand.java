package ru.potapov.tm.command;

import ru.potapov.tm.bootstrap.Bootstrap;

public class UserDeleteCommand extends AbstractCommand {
    public UserDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-delete";
    }

    @Override
    public String getDescription() {
        return "Deletes a user";
    }

    @Override
    public void execute() {
        super.execute();
    }
}
