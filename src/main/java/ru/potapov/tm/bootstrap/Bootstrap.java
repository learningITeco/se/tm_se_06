package ru.potapov.tm.bootstrap;

import ru.potapov.tm.command.*;
import ru.potapov.tm.repository.ProjectRepository;
import ru.potapov.tm.repository.TaskRepository;
import ru.potapov.tm.repository.UserRepository;
import ru.potapov.tm.service.ProjectService;
import ru.potapov.tm.service.TaskService;
import ru.potapov.tm.service.TerminalService;
import ru.potapov.tm.service.UserService;
import java.util.*;

public class Bootstrap implements ServiceLocator {
    private ProjectRepository projectRepository     = new ProjectRepository();
    private TaskRepository taskRepository           = new TaskRepository();
    private UserRepository userRepository           = new UserRepository();

    private ProjectService projectService           = new ProjectService(projectRepository);
    private TaskService taskService                 = new TaskService(taskRepository, projectRepository);
    private UserService userService                 = new UserService(userRepository);
    private TerminalService terminalService         = new TerminalService(this);

    public Bootstrap() {}

    public void init(){
        try {
            start();
        }catch (Exception e){
            terminalService.printlnArbitraryMassage("Something went wrong...");
            e.printStackTrace();
        }
    }

    private void start() throws Exception{
        terminalService.printlnArbitraryMassage("*** WELCOME TO TASK MANAGER! ***");
        String command = "";
        while (!"exit".equals(command)){
            command = terminalService.readLine("\nInsert your command in low case or command <help>:");
            execute(command);
        }
    }

    private void execute(String command) throws Exception{
        if ( Objects.isNull(command) || command.isEmpty())
            return;

        AbstractCommand abstractCommand = terminalService.getMapCommands().get(command);

        if (Objects.isNull(abstractCommand))
            return;

        abstractCommand.execute();
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }
}