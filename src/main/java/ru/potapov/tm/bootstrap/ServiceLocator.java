package ru.potapov.tm.bootstrap;

import ru.potapov.tm.service.ProjectService;
import ru.potapov.tm.service.TaskService;
import ru.potapov.tm.service.TerminalService;
import ru.potapov.tm.service.UserService;

public interface ServiceLocator {
    ProjectService  getProjectService();
    TaskService     getTaskService();
    TerminalService getTerminalService();
    UserService     getUserService();
}
