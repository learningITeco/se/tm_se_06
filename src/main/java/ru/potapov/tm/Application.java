package ru.potapov.tm;

import ru.potapov.tm.bootstrap.Bootstrap;

/**
 * Application ***
 * v 1.0.3
 */
public class Application {
    Bootstrap bootstrap;

    public Application() {
        bootstrap       = new Bootstrap();
    }

    public static void main(String[] args) {
        new Application().go();
    }

    public void go() {
        bootstrap.init();
    }
}
