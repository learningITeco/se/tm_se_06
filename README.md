﻿# ПРОЕКТНАЯ ДОКУМЕНТАЦИЯ
1. SOFTWARE: task-manager
2. СТЕК ТЕХНОЛОГИЙ: Java SE8 (Core, Collections)
3. РАЗРАБОТЧИК: Потапов Дмитрий, dmitriyp.potapov@gmail.com, +7 937 173-85-05
4. КОМАНДЫ ДЛЯ СБОРКИ ПРИЛОЖЕНИЯ: 
    1. MAVEN: mvn install
    2. JAVAC:
        1. ```"C:\Program Files\Java\jdk1.8.0_192\bin\javac" ru\potapov\tm\se02\*.java```
        2. ```"C:\Program Files\Java\jdk1.8.0_192\bin\jar" cvfm tm-0.jar manifest.txt *```
5. КОМАНДЫ ДЛЯ ЗАПУСКА ПРИЛОЖЕНИЯ: ```java -jar tm-0.jar```